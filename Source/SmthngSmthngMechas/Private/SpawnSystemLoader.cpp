// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnSystemLoader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <exception>
#include <stdexcept>

using namespace std;

// Sets default values
ASpawnSystemLoader::ASpawnSystemLoader()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	levelName = FString("Test5");
}

// Called when the game starts or when spawned
void ASpawnSystemLoader::BeginPlay()
{
	Super::BeginPlay();
	
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::White, TEXT("SpawnSystemLoader opening file..."));
	//
	//FString myFile = FPaths::ProjectContentDir() + "/" + "testfile.txt";

	//TArray<FString> lines;
	//FFileHelper::LoadFileToStringArray(lines, *myFile);
	//
	//for (auto const& line : lines) {
	//	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::White, line);
	//}
}

// Called every frame
void ASpawnSystemLoader::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

