// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "PaperSprite.h"
#include "SpawnSystemLoader.generated.h"

UENUM(BlueprintType)
enum class EnemyId : uint8 {
	BLOCKER UMETA(DisplayName = "Blocker"),
	SNIPER UMETA(DisplayName = "Sniper"),
	TANK UMETA(DisplayName = "Tank"),
	BLOB UMETA(DisplayName = "Blob"),

	BOSS1 UMETA(DisplayName = "Boss 1"),
	BOSS2 UMETA(DisplayName = "Boss 2"),
	BOSS3 UMETA(DisplayName = "Boss 3")
};

USTRUCT(BlueprintType)
struct FEnemySpawnData {
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float x;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float y;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EnemyId id UMETA(DisplayName = "Enemy Id");

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector movement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float clockOffset;
};


USTRUCT(BlueprintType)
struct FEnemySpawnPattern {
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FEnemySpawnData> spawnDatas UMETA(DisplayName = "Spawn Datas");
};




USTRUCT(BlueprintType)
struct FBackgroundLayer {
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float depth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<UPaperSprite*>backgroundAssetNames;
};

USTRUCT(BlueprintType)
struct FLevelData {
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FEnemySpawnPattern bossPattern UMETA(DisplayName = "Boss Pattern");

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FEnemySpawnPattern> patterns;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float maxBossHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float baseSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FBackgroundLayer> background;
};


#define FETCH_SPRITE(path) Cast<UPaperSprite>(StaticLoadObject(UPaperSprite::StaticClass(), NULL, TEXT(path)))
#define BACKGROUND_LAYER(depth, ...) {depth, {__VA_ARGS__}}
#define SPAWN_PATTERN(...) {{__VA_ARGS__}}
#define VEC2D(x, y) {x, 0, y}

#define OOS 260 // Out Of Sight X

#define SPAWN(x, y, id) {x, y, id, VEC2D(0, 0), 0}
#define SNIPER(x, y) {x, y, EnemyId::SNIPER, VEC2D(0, 0), 0}


UCLASS()
class SMTHNGSMTHNGMECHAS_API ASpawnSystemLoader : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Configuration")
	FString levelName;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Configuration")

	
	// Sets default values for this actor's properties
	ASpawnSystemLoader();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
	TArray<FEnemySpawnPattern> PatternsList = {
		// Spawn pattern
		SPAWN_PATTERN(
			{32, 110, EnemyId::BLOB, VEC2D(0, -20), 1},
			{32, 145, EnemyId::BLOB, VEC2D(0, -20), 1},
			{32, 180, EnemyId::BLOB, VEC2D(0, -20), 1},

			{75, -138, EnemyId::BLOB, VEC2D(0, 20), 0},
			{75, -173, EnemyId::BLOB, VEC2D(0, 20), 0},
			{75, -208, EnemyId::BLOB, VEC2D(0, 20), 0},

			{150, 278, EnemyId::BLOCKER, VEC2D(0, -40), 0},
			{150, 307, EnemyId::BLOCKER, VEC2D(0, -40), 0},
			{150, 339, EnemyId::BLOCKER, VEC2D(0, -40), 0},
			{150, 368, EnemyId::BLOCKER, VEC2D(0, -40), 0},
		),
		SPAWN_PATTERN(
			{OOS, 80, EnemyId::BLOCKER, VEC2D(-40, 0), 0},
			{OOS + 1 * 30, 80, EnemyId::BLOCKER, VEC2D(-40, 0), 0},
			{OOS + 2 * 30, 80, EnemyId::BLOCKER, VEC2D(-40, 0), 0},
			{OOS + 3 * 30, 80, EnemyId::BLOCKER, VEC2D(-40, 0), 0},

			{OOS, -80, EnemyId::BLOCKER, VEC2D(-40, 0), 0},
			{OOS + 1 * 30, -80, EnemyId::BLOCKER, VEC2D(-40, 0), 0},
			{OOS + 2 * 30, -80, EnemyId::BLOCKER, VEC2D(-40, 0), 0},
			{OOS + 3 * 30, -80, EnemyId::BLOCKER, VEC2D(-40, 0), 0},

			SPAWN(OOS+20, 34, EnemyId::TANK),
			SPAWN(OOS+20, -34, EnemyId::TANK)
		),
		SPAWN_PATTERN(
			{150, 278, EnemyId::BLOCKER, VEC2D(0, -40), 0},
			{150, 307, EnemyId::BLOCKER, VEC2D(0, -40), 0},
			{150, 339, EnemyId::BLOCKER, VEC2D(0, -40), 0},
			{150, 368, EnemyId::BLOCKER, VEC2D(0, -40), 0},

			{62, -138, EnemyId::BLOB, VEC2D(0, 20), 0},
			{62, -195, EnemyId::BLOB, VEC2D(0, 20), 0},
			{62, -252, EnemyId::BLOB, VEC2D(0, 20), 0},
			{62, -310, EnemyId::BLOB, VEC2D(0, 20), 0},

			SNIPER(OOS, 58),
			SNIPER(OOS, 0),
			SNIPER(OOS, -94)
		),
		SPAWN_PATTERN(
			{-167, 140, EnemyId::BLOB, VEC2D(20, -20), 0},
			{-253, 212, EnemyId::BLOB, VEC2D(20, -20), 0},
			{-339, 283, EnemyId::BLOB, VEC2D(20, -20), 0},

			{-209, -184, EnemyId::BLOB, VEC2D(20, 20), 0},
			{-296, -271, EnemyId::BLOB, VEC2D(20, 20), 0},
			{-383, -341, EnemyId::BLOB, VEC2D(20, 20), 0},

			SPAWN(OOS + 20, 40, EnemyId::TANK),
			SPAWN(OOS + 20, -40, EnemyId::TANK)
		),
		SPAWN_PATTERN(
			SNIPER(OOS, 100),
			SNIPER(OOS, -100),
			SPAWN(OOS + 20, 80, EnemyId::TANK),
			SPAWN(OOS + 20, 00, EnemyId::TANK),
			SPAWN(OOS + 20, -40, EnemyId::TANK)
		),

		SPAWN_PATTERN(
			{30, 203, EnemyId::BLOCKER, VEC2D(30, -30), 0 },
			{15, 218, EnemyId::BLOCKER, VEC2D(30, -30), 0 },
			{0, 233, EnemyId::BLOCKER, VEC2D(30, -30), 0 },
			{-15, 248, EnemyId::BLOCKER, VEC2D(30, -30), 0 },
			{-30, 263, EnemyId::BLOCKER, VEC2D(30, -30), 0 },
			{30, -203, EnemyId::BLOCKER, VEC2D(30, 30), 0 },
			{15, -218, EnemyId::BLOCKER, VEC2D(30, 30), 0 },
			{0, -233, EnemyId::BLOCKER, VEC2D(30, 30), 0 },
			{-15, -248, EnemyId::BLOCKER, VEC2D(30, 30), 0 },
			{-30, -263, EnemyId::BLOCKER, VEC2D(30, 30), 0 },
			{-30, 203, EnemyId::BLOCKER, VEC2D(-30, -30), 0 },
			{-15, 218, EnemyId::BLOCKER, VEC2D(-30, -30), 0 },
			{0, 233, EnemyId::BLOCKER, VEC2D(-30, -30), 0 },
			{15, 248, EnemyId::BLOCKER, VEC2D(-30, -30), 0 },
			{30, 263, EnemyId::BLOCKER, VEC2D(-30, -30), 0 },
			{-30, -203, EnemyId::BLOCKER, VEC2D(-30, 30), 0 },
			{-15, -218, EnemyId::BLOCKER, VEC2D(-30, 30), 0 },
			{0, -233, EnemyId::BLOCKER, VEC2D(-30, 30), 0 },
			{15, -248, EnemyId::BLOCKER, VEC2D(-30, 30), 0 },
			{30, -263, EnemyId::BLOCKER, VEC2D(-30, 30), 0 },
		),

		SPAWN_PATTERN(
			{OOS + 20, 0, EnemyId::BLOB, VEC2D(-40, 0), 0},
			{OOS + 50, 0, EnemyId::BLOB, VEC2D(-40, 0), 0},
			{OOS + 80, 0, EnemyId::BLOB, VEC2D(-40, 0), 0},
			{OOS + 110, 0, EnemyId::BLOB, VEC2D(-40, 0), 0},
			{OOS + 140, 0, EnemyId::BLOB, VEC2D(-40, 0), 0},
			SNIPER(OOS, 100),
			SNIPER(OOS, -100),
			SNIPER(OOS, 150),
			SNIPER(OOS, -150),
		),

		SPAWN_PATTERN(
			{ 200, 203, EnemyId::BLOB, VEC2D(-20, -40), 0 },
			{ 200, 218, EnemyId::BLOB, VEC2D(-20, -40), 0 },
			{ 200, 233, EnemyId::BLOB, VEC2D(-20, -40), 0 },
			{ 200, 248, EnemyId::BLOB, VEC2D(-20, -40), 0 },
			{ 230, 203, EnemyId::BLOCKER, VEC2D(-20, -40), 0 },
			{ 230, 218, EnemyId::BLOCKER, VEC2D(-20, -40), 0 },
			{ 230, 233, EnemyId::BLOCKER, VEC2D(-20, -40), 0 },
			{ 230, 248, EnemyId::BLOCKER, VEC2D(-20, -40), 0 },
			{ OOS + 20, 100, EnemyId::TANK, VEC2D(0, 0), 3 },
			{ OOS + 20, 0, EnemyId::TANK, VEC2D(0, 0), 5 },
			{ OOS + 20, -100, EnemyId::TANK, VEC2D(0, 0), 7 },
		),

		SPAWN_PATTERN(
			{ 300, 200, EnemyId::BLOB, VEC2D(-30, -30), 0 },
			{ 315, 215, EnemyId::BLOB, VEC2D(-30, -30), 0 },
			{ 330, 230, EnemyId::BLOB, VEC2D(-30, -30), 0 },
			{ 345, 245, EnemyId::BLOB, VEC2D(-30, -30), 0 },

			{ -300, 200, EnemyId::BLOB, VEC2D(30, -30), 1 },
			{ -315, 215, EnemyId::BLOB, VEC2D(30, -30), 1 },
			{ -330, 230, EnemyId::BLOB, VEC2D(30, -30), 1 },
			{ -345, 245, EnemyId::BLOB, VEC2D(30, -30), 1 },

			{ -300, -200, EnemyId::BLOB, VEC2D(30, 30), 2 },
			{ -315, -215, EnemyId::BLOB, VEC2D(30, 30), 2 },
			{ -330, -230, EnemyId::BLOB, VEC2D(30, 30), 2 },
			{ -345, -245, EnemyId::BLOB, VEC2D(30, 30), 2 },

			{ 300, -200, EnemyId::BLOB, VEC2D(-30, 30), 3 },
			{ 315, -215, EnemyId::BLOB, VEC2D(-30, 30), 3 },
			{ 330, -230, EnemyId::BLOB, VEC2D(-30, 30), 3 },
			{ 345, -245, EnemyId::BLOB, VEC2D(-30, 30), 3 },
		),

		SPAWN_PATTERN(
			{ OOS + 20, 100, EnemyId::BLOB, VEC2D(-30, 0), 1 },
			{ OOS + 40, 100, EnemyId::BLOB, VEC2D(-30, 0), 1 },
			{ OOS + 60, 100, EnemyId::BLOB, VEC2D(-30, 0), 1 },
			{ OOS + 80, 100, EnemyId::BLOB, VEC2D(-30, 0), 1 },

			{ OOS + 20, -100, EnemyId::BLOB, VEC2D(-30, 0), 1 },
			{ OOS + 40, -100, EnemyId::BLOB, VEC2D(-30, 0), 1 },
			{ OOS + 60, -100, EnemyId::BLOB, VEC2D(-30, 0), 1 },
			{ OOS + 80, -100, EnemyId::BLOB, VEC2D(-30, 0), 1 },

			{ OOS + 20, 100, EnemyId::BLOCKER, VEC2D(-50, -25), 2 },
			{ OOS + 45, 125, EnemyId::BLOCKER, VEC2D(-50, -25), 2 },
			{ OOS + 70, 150, EnemyId::BLOCKER, VEC2D(-50, -25), 2 },
			{ OOS + 95, 175, EnemyId::BLOCKER, VEC2D(-50, -25), 2 },

			{ OOS + 20, -100, EnemyId::BLOCKER, VEC2D(-50, 25), 2 },
			{ OOS + 45, -125, EnemyId::BLOCKER, VEC2D(-50, 25), 2 },
			{ OOS + 70, -150, EnemyId::BLOCKER, VEC2D(-50, 25), 2 },
			{ OOS + 95, -175, EnemyId::BLOCKER, VEC2D(-50, 25), 2 },

			{ OOS + 20, 50, EnemyId::TANK, VEC2D(0, 0), 3 },
			{ OOS + 20, -50, EnemyId::TANK, VEC2D(0, 0), 3 },
		),
		SPAWN_PATTERN(
			{ 200, -200, EnemyId::BLOCKER, VEC2D(0, 40), 1 },
			{ 200, -220, EnemyId::BLOCKER, VEC2D(0, 40), 1 },
			{ 200, -240, EnemyId::BLOCKER, VEC2D(0, 40), 1 },
			{ 200, -260, EnemyId::BLOCKER, VEC2D(0, 40), 1 },

			{ 100, 200, EnemyId::BLOCKER, VEC2D(0, -40), 3 },
			{ 100, 220, EnemyId::BLOCKER, VEC2D(0, -40), 3 },
			{ 100, 240, EnemyId::BLOCKER, VEC2D(0, -40), 3 },
			{ 100, 260, EnemyId::BLOCKER, VEC2D(0, -40), 3 },

			{ 0, -200, EnemyId::BLOCKER, VEC2D(0, 40), 5 },
			{ 0, -220, EnemyId::BLOCKER, VEC2D(0, 40), 5 },
			{ 0, -240, EnemyId::BLOCKER, VEC2D(0, 40), 5 },
			{ 0, -260, EnemyId::BLOCKER, VEC2D(0, 40), 5 },

			{ -75, -200, EnemyId::BLOCKER, VEC2D(0, 40), 7 },
			{ -75, -220, EnemyId::BLOCKER, VEC2D(0, 40), 7 },
			{ -75, -240, EnemyId::BLOCKER, VEC2D(0, 40), 7 },
			{ -75, -260, EnemyId::BLOCKER, VEC2D(0, 40), 7 },
			{ -50, 200, EnemyId::BLOCKER, VEC2D(0, -40), 7 },
			{ -50, 220, EnemyId::BLOCKER, VEC2D(0, -40), 7 },
			{ -50, 240, EnemyId::BLOCKER, VEC2D(0, -40), 7 },
			{ -50, 260, EnemyId::BLOCKER, VEC2D(0, -40), 7 },

			SNIPER(OOS, 100),
			SNIPER(OOS, -100),
			SNIPER(OOS, 150),
			SNIPER(OOS, -150),
		),

		SPAWN_PATTERN(
			{ OOS + 20, 150, EnemyId::TANK, VEC2D(0, 0), 1 },
			{ OOS + 20, -150, EnemyId::TANK, VEC2D(0, 0), 1 },
			{ OOS + 20, 100, EnemyId::TANK, VEC2D(0, 0), 1 },
			{ OOS + 20, -100, EnemyId::TANK, VEC2D(0, 0), 1 },
			SNIPER(OOS, 50),
			SNIPER(OOS, 0),
			SNIPER(OOS, -50),

			{ OOS - 40, 200, EnemyId::BLOB, VEC2D(0, -20), 3 },
			{ OOS - 40, 220, EnemyId::BLOB, VEC2D(0, -20), 3 },
			{ OOS - 40, 240, EnemyId::BLOB, VEC2D(0, -20), 3 },
			{ OOS - 40, 260, EnemyId::BLOB, VEC2D(0, -20), 3 },

			{ OOS - 60, 200, EnemyId::BLOB, VEC2D(0, -20), 3 },
			{ OOS - 60, 220, EnemyId::BLOB, VEC2D(0, -20), 3 },
			{ OOS - 60, 240, EnemyId::BLOB, VEC2D(0, -20), 3 },
			{ OOS - 60, 260, EnemyId::BLOB, VEC2D(0, -20), 3 },
		),

		SPAWN_PATTERN(
			{ -20, 200, EnemyId::BLOB, VEC2D(0, -30), 1 },
			{ -20, 220, EnemyId::BLOB, VEC2D(0, -30), 1 },
			{ -20, 240, EnemyId::BLOB, VEC2D(0, -30), 1 },
			{ -20, 260, EnemyId::BLOB, VEC2D(0, -30), 1 },

			{ 20, 200, EnemyId::BLOB, VEC2D(0, 30), 1 },
			{ 20, 220, EnemyId::BLOB, VEC2D(0, 30), 1 },
			{ 20, 240, EnemyId::BLOB, VEC2D(0, 30), 1 },
			{ 20, 260, EnemyId::BLOB, VEC2D(0, 30), 1 },

			{ OOS + 20, 20, EnemyId::BLOCKER, VEC2D(-50, 0), 2 },
			{ OOS + 45, 20, EnemyId::BLOCKER, VEC2D(-50, 0), 2 },
			{ OOS + 70, 20, EnemyId::BLOCKER, VEC2D(-50, 0), 2 },
			{ OOS + 95, 20, EnemyId::BLOCKER, VEC2D(-50, 0), 2 },

			{ OOS + 20, -20, EnemyId::BLOCKER, VEC2D(-50, 0), 2 },
			{ OOS + 45, -20, EnemyId::BLOCKER, VEC2D(-50, 0), 2 },
			{ OOS + 70, -20, EnemyId::BLOCKER, VEC2D(-50, 0), 2 },
			{ OOS + 95, -20, EnemyId::BLOCKER, VEC2D(-50, 0), 2 },
		)

	};




	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
	TArray<FEnemySpawnPattern> BossPatternsList = {
		SPAWN_PATTERN({OOS, 0, EnemyId::BOSS1, VEC2D(0, 0), 0}),
		SPAWN_PATTERN({OOS+20, 0, EnemyId::BOSS2, VEC2D(0, 0), 0}),
		SPAWN_PATTERN({OOS+50, 0, EnemyId::BOSS3, VEC2D(0, 0), 0})
	};


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		TArray<FLevelData> Levels = {
			{
				BossPatternsList[0],
				{

					PatternsList[0],
					PatternsList[1],
					PatternsList[2],
					PatternsList[3],
					PatternsList[4]
			},
			100,
			400,
			{
				BACKGROUND_LAYER(12, 
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/Layer5/parallax_debrisA_Sprite.parallax_debrisA_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/Layer5/parallax_debrisB_Sprite.parallax_debrisB_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/Layer5/parallax_debrisC_Sprite.parallax_debrisC_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/Layer5/parallax_debrisD_Sprite.parallax_debrisD_Sprite'")
				),
				BACKGROUND_LAYER(22,
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/layer4/parallax_rockA_Sprite.parallax_rockA_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/layer4/parallax_rockB_Sprite.parallax_rockB_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/layer4/parallax_rockC_Sprite.parallax_rockC_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/layer4/parallax_rockD_Sprite.parallax_rockD_Sprite'")
				),
				BACKGROUND_LAYER(32, FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/Layer3/parallax_gaz_Sprite.parallax_gaz_Sprite'")),
				BACKGROUND_LAYER(60, FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/Layer2/parallax_backstar_Sprite.parallax_backstar_Sprite'")),
				BACKGROUND_LAYER(80, FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space/Layer1/baground_space_Sprite.baground_space_Sprite'"))
			}
		},

		{
			BossPatternsList[1],
			{
				PatternsList[0],
				PatternsList[1],
				PatternsList[2],
				PatternsList[3],
				PatternsList[4],
				PatternsList[5],
				PatternsList[6]
			},
			120,
			400,
			{
				BACKGROUND_LAYER(12,
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/Layer5/parallax_debrisA2_Sprite.parallax_debrisA2_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/Layer5/parallax_debrisB2_Sprite.parallax_debrisB2_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/Layer5/parallax_debrisC2_Sprite.parallax_debrisC2_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/Layer5/parallax_debrisD2_Sprite.parallax_debrisD2_Sprite'")
				),
				BACKGROUND_LAYER(22,
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/layer4/parallax_rockA2_Sprite.parallax_rockA2_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/layer4/parallax_rockB2_Sprite.parallax_rockB2_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/layer4/parallax_rockC2_Sprite.parallax_rockC2_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/layer4/parallax_rockD2_Sprite.parallax_rockD2_Sprite'")
				),
				BACKGROUND_LAYER(32, FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/Layer3/parallax_gaz2_Sprite.parallax_gaz2_Sprite'")),
				BACKGROUND_LAYER(60, FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/Layer2/parallax_backstar2_Sprite.parallax_backstar2_Sprite'")),
				BACKGROUND_LAYER(80, FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space2/Layer1/baground_space2_Sprite.baground_space2_Sprite'"))
			}
		},

		{
			BossPatternsList[2],
			{
				PatternsList[0],
				PatternsList[1],
				PatternsList[2],
				PatternsList[3],
				PatternsList[4],
				PatternsList[5],
				PatternsList[6],
				PatternsList[7],
				PatternsList[8],
				PatternsList[9],
				PatternsList[10],
				PatternsList[11],
				PatternsList[12]
			},
			150,
			500,
			{
				BACKGROUND_LAYER(12,
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/Layer5/parallax_debrisA3_Sprite.parallax_debrisA3_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/Layer5/parallax_debrisB3_Sprite.parallax_debrisB3_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/Layer5/parallax_debrisC3_Sprite.parallax_debrisC3_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/Layer5/parallax_debrisD3_Sprite.parallax_debrisD3_Sprite'")
				),
				BACKGROUND_LAYER(22,
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/layer4/parallax_rockA3_Sprite.parallax_rockA3_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/layer4/parallax_rockB3_Sprite.parallax_rockB3_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/layer4/parallax_rockC3_Sprite.parallax_rockC3_Sprite'"),
					FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/layer4/parallax_rockD3_Sprite.parallax_rockD3_Sprite'")
				),
				BACKGROUND_LAYER(32, FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/Layer3/parallax_gaz3_Sprite.parallax_gaz3_Sprite'")),
				BACKGROUND_LAYER(60, FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/Layer2/parallax_backstar3_Sprite.parallax_backstar3_Sprite'")),
				BACKGROUND_LAYER(80, FETCH_SPRITE("PaperSprite'/Game/Backgrounds/Space3/Layer1/baground_space3_Sprite.baground_space3_Sprite'"))
			}
		}
	};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};